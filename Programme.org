#+TITLE: Programme de l'atelier ={rdss}=

** Introduction
- Rappels sur la procédure de "/diagnose sexuelle secondaire/" ([[http://onlinelibrary.wiley.com/doi/abs/10.1002/%28SICI%291099-1212%28199901/02%299%3A1%3C39%3A%3AAID-OA458%3E3.0.CO%3B2-V][Murail et al., 1999]])
- Présentation générale de ={rdss}= ([[https://doi.org/10.1002/oa.2957][Santos, 2021]])

** Concevoir et importer les données
- Formats des fichiers d'entrée
- Erreurs à éviter dans la mise en forme des données

** Repérer et gérer les données manquantes
- Représenter et comprendre la répartition des données manquantes dans l'échantillon d'apprentissage
- Filtrer l'échantillon d'apprentissage pour obtenir un sous-ensemble exploitable

** Repérer et gérer les outliers et erreurs de saisie
- Techniques simples pour repérer de potentiels outliers
- Vérifier (graphiquement) certaines des hypothèses des modèles

** Effectuer l'estimation du sexe
- Choisir l'algorithme le plus adapté en fonction des spécificités des données
- Discussion sur les seuils de probabilité à posteriori ; mise en perspective par rapport aux débats récents sur le sujet (e.g., [[https://doi.org/10.1016/j.forsciint.2020.110273][Jerkovic et al., 2020]] ; [[https://doi.org/10.1016/j.fsisyn.2021.100202][Morrison et al., 2021]])
- Sauvegarder les résultats de l'estimation du sexe

** Analyser l'impact des données manquantes
- Procéder à une analyse (basique) de sensibilité avec ={rdss}=

** Prolongements et questions diverses
- Application sur les données des participants
- ={rdss}= en ligne de commande (pour ceux qui ont pu installer la vignette du package, exécuter =vignette("intro_rdss", package = "rdss")= pour accéder à une documentation détaillée)
